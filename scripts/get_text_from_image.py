import cv2
import tesserocr
from PIL import Image

img = cv2.imread('images.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
threshold = 180 # to be determined
_, img_binarized = cv2.threshold(img, threshold, 255, cv2.THRESH_BINARY)
pil_img = Image.fromarray(img_binarized)
text_from_ocr = tesserocr.image_to_text(pil_img)
print(text_from_ocr.split())
