import os
import pickle
from flask import Flask
import config as config
from v1.extract_text import api


ENVIRONMENT = 'Development'
#ENVIRONMENT = 'Production'

app = Flask(__name__)
app.config.from_object(f'config.{ENVIRONMENT}')
app.register_blueprint(api)


if __name__ == '__main__':
	app.run(host="127.0.0.1", port=5001, debug=True)

