import os
import re
import cv2
import pytesseract
import json
import time
import traceback
from PIL import Image
from flask import Blueprint, request, jsonify
from utils import generate_id, Logger, Decorators, Response



api = Blueprint('api', __name__, url_prefix='/api/v1')

@api.route('/verify', methods=['POST'])
def verify_images():
	request_id = generate_id()
	logger = Logger(request_id).event("VERIFY_IMAGES")
	try:
		file = request.files['file']
	except Exception as e:
		msg = "No file attached"
		logger.error(msg)
		traceback.print_exc()
		return Response.input_error(msg=msg)
	try:
		text_list = extract_text(file, request_id)
		if not text_list:
			return Response.ok_response(msg="No matched text")
		matches = check_text_matches(text_list)
		if matches:
			return Response.ok_response(msg="Extracted matches", data=matches)
		return Response.ok_response(msg="No matched text")

	except Exception as e:
		msg = "An error occurred"
		logger.error(msg)
		traceback.print_exc()
		return Response.system_error(msg=msg)


def extract_text(file, request_id):
	file_ext = os.path.splitext(file.filename)[1]
	file_name = f'{request_id}{file_ext}'
	file.save(os.path.join('temp/', file_name))

	img = cv2.imread(f'temp/{file_name}')
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	threshold = 180
	_, img_binarized = cv2.threshold(img, threshold, 255, cv2.THRESH_BINARY)
	pil_img = Image.fromarray(img_binarized)
	text_from_ocr = pytesseract.image_to_string(pil_img).split()
	print(text_from_ocr)
	os.remove(f'temp/{file_name}')
	return text_from_ocr


def check_text_matches(text_list):
	matches = []
	regex = re.compile("((?:\+\d{2}[-\.\s]??|\d{4}[-\.\s]??)?(?:\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4}))")
	for text in text_list:
		if regex.search(text):
			matches.append(text)
	return matches
